package com.android.test.application

import android.app.Application


/**
 * Application class to get the context from anywhere in the application
 */
class MyApplication : Application() {

    companion object {
        lateinit var instance: MyApplication
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }


}