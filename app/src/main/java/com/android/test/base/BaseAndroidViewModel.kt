package com.android.test.base

import android.app.Application
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.AndroidViewModel
import com.android.test.database.AppDatabase


/**
 * Description : create base view Model
 */

abstract class BaseAndroidViewModel(application: Application) : AndroidViewModel(application) {
    private var isLoading = ObservableBoolean(false)
    var mDB: AppDatabase? = null


    init {
        mDB = AppDatabase.getInstance()

    }


    fun setIsLoading(isLoading: Boolean) {
        this.isLoading.set(isLoading)
    }


}


