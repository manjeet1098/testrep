package com.android.test.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Description : create base view holder
 */

abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun onBind(position: Int)


}