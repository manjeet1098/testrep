package com.android.test.bindingAdapter

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler

/*
 * Description : TO implement class Bind data with xml layouts
 */
class BindingAdapter {
    /*
     * Description : Bind Data with xml layout
     */
    companion object {
        /**
         * Method to zoom the image
         */
        @BindingAdapter("product_photo")
        @JvmStatic
        fun zoomImage(view: ImageView, imageUrl: Int) {
            view.setOnTouchListener(ImageMatrixTouchHandler(view.context))

        }


    }
}