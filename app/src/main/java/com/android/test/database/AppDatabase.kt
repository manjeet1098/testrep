package com.android.test.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.android.test.R
import com.android.test.application.MyApplication
import com.android.test.database.entity.ProductEntity
import com.tap5050.database.dao.ProductDao


// Annotates class to be a Room Database with a table (entity) of the Word class

@Database(
    entities = [ProductEntity::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun productDao(): ProductDao

    companion object {
        private var mAppDatabase: AppDatabase? = null

        /**
        Description:This method is used to get database instance
         **/
        fun getInstance(): AppDatabase? {
            if (mAppDatabase == null) {
                synchronized(AppDatabase::class) {
                    mAppDatabase = Room.databaseBuilder(
                        MyApplication.instance,
                        AppDatabase::class.java,
                        MyApplication.instance.getString(
                            R.string.test_db
                        )
                    ).build()

                    if (DatabaseRepository(
                            mAppDatabase!!
                        ).getProductList()!!.size == 0
                    ) {
                        DatabaseRepository(
                            mAppDatabase!!
                        ).insertDefaultProducts()
                    }
                }
            }

            return mAppDatabase
        }

    }


}



