package com.android.test.database

import com.android.test.R
import com.android.test.application.MyApplication
import com.android.test.database.entity.ProductEntity
import com.android.test.model.ProductModel
import com.android.test.model.ProductResponse
import com.android.test.utils.AppConstants
import com.android.test.utils.AppConstants.Companion.PRODUCT_ARRAY_KEY
import com.android.test.utils.AppConstants.Companion.PRODUCT_DESCRIPTION_KEY
import com.android.test.utils.AppConstants.Companion.PRODUCT_NAME_KEY
import com.android.test.utils.AppConstants.Companion.PRODUCT_REGULAR_PRICE_KEY
import com.android.test.utils.AppConstants.Companion.PRODUCT_SALE_PRICE_KEY
import com.android.test.utils.AppConstants.Companion.PRODUCT_STORE_KEY
import com.google.gson.Gson
import kotlinx.coroutines.runBlocking
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject


/**
 * This class is used to deal with products for CRUD operations in local database and TF card database.
 */
class DatabaseRepository(mDB: AppDatabase) {
    private var appDB: AppDatabase? = mDB

    /**
     * Method to parse the  entity fetched from local database to model class.
     */
    private fun parseLogModelFromEntity(logEntityList: List<ProductEntity>?): ArrayList<ProductModel>? {
        val productModelList = ArrayList<ProductModel>()
        logEntityList?.forEach { productEntity ->
            val productModel = ProductModel()
            productModel.productId = productEntity.productId
            productModel.name = productEntity.name
            productModel.description = productEntity.description
            productModel.salePrice = productEntity.salePrice
            productModel.regularPrice = productEntity.regularPrice
            productModel.productPhoto = productEntity.productPhoto
            productModel.stores = productEntity.stores
            productModel.colors = productEntity.colors

            productModelList.add(productModel)
        }
        return productModelList
    }

    /**
     * Method to parse the product model to entity.
     */
    private fun parseLogEntityFromModel(productModel: ProductModel): ProductEntity {
        val productEntity = ProductEntity()
        productEntity.productId = productModel.productId
        productEntity.name = productModel.name
        productEntity.description = productModel.description
        productEntity.regularPrice = productModel.regularPrice
        productEntity.salePrice = productModel.salePrice
        productEntity.stores = productModel.stores
        productEntity.colors = productModel.colors
        productEntity.productPhoto = productModel.productPhoto

        return productEntity
    }

    /**
     * Method to fetch the products from local database.
     */
    fun getProductList(): ArrayList<ProductModel>? {
        return runBlocking {
            return@runBlocking parseLogModelFromEntity(
                appDB?.productDao()?.getProducts()
            )
        }
    }

    /**
     * Method to insert product in local database.
     */
    fun insertProducts(productModel: ProductModel) {
        runBlocking {
            appDB?.productDao()?.insertProduct(parseLogEntityFromModel(productModel))

        }
    }

    /**
     * Method to insert some default product in local database.
     */
    fun insertDefaultProducts() {
        // make records as json object and then parse it to insert in database
        val result = getObject(
            makJsonObject(
                MyApplication.instance.resources.getIntArray(R.array.product_id_list),
                MyApplication.instance.resources.getStringArray(R.array.name_list),
                MyApplication.instance.resources.getStringArray(R.array.desc_list),
                MyApplication.instance.resources.getStringArray(R.array.sale_price_list),
                MyApplication.instance.resources.getStringArray(R.array.stores_list),
                MyApplication.instance.resources.getStringArray(R.array.regular_price_list)
            ).toString(), ProductResponse::class.java
        )
        if (result is ProductResponse) {
            val productList = result.product
            productList.forEach {
                runBlocking {
                    appDB?.productDao()?.insertProduct(parseLogEntityFromModel(it))

                }
            }

        }
    }

    /**
     * @description Method is used to convert json data into respective POJO class.
     */
    private fun <T> getObject(response: String, instance: Class<T>): Any? {
        return Gson().fromJson(response, instance)
    }

    /**
     * Method to update product in local database.
     */
    fun updateProduct(oldName: String, productModel: ProductModel) {
        runBlocking {
            appDB?.productDao()?.updateProduct(
                oldName,
                productModel.name!!,
                productModel.description!!,
                productModel.salePrice!!,
                productModel.regularPrice!!,
                productModel.stores!!,
                productModel.productId!!
            )

        }
    }


    /**
     * Method to delete products in local database.
     */
    fun deleteProducts(productId: Long) {
        runBlocking {
            appDB?.productDao()?.deleteProduct(productId)
        }

    }
    /**
     * Method to wrap data in json.
     */

    @Throws(JSONException::class)
    fun makJsonObject(
        id: IntArray,
        name: Array<String?>,
        description: Array<String?>,
        salePrice: Array<String?>,
        stores: Array<String?>,
        regularPrice: Array<String?>
    ): JSONObject? {
        var obj: JSONObject? = null
        val jsonArray = JSONArray()
        for (i in id.indices) {
            obj = JSONObject()
            try {
                obj.put(AppConstants.PRODUCT_ID_KEY, id[i])
                obj.put(PRODUCT_NAME_KEY, name[i])
                obj.put(PRODUCT_DESCRIPTION_KEY, description[i])
                obj.put(PRODUCT_SALE_PRICE_KEY, salePrice[i])
                obj.put(PRODUCT_REGULAR_PRICE_KEY, regularPrice[i])
                obj.put(PRODUCT_STORE_KEY, stores[i])

            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
            jsonArray.put(obj)
        }
        val finalObject = JSONObject()
        finalObject.put(PRODUCT_ARRAY_KEY, jsonArray)
        return finalObject
    }


}