package com.tap5050.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.android.test.database.entity.ProductEntity

/**
 * This class is used as dao class to deal with products.
 */
@Dao
abstract class ProductDao {
    // insert products
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertProduct(productEntity: ProductEntity)

    // fetch all products and return as a list of entity
    @Query("SELECT * FROM product_entity")
    abstract suspend fun getProducts(): List<ProductEntity>


    //delete products
    @Query("DELETE FROM product_entity where product_id = :productId ")
    abstract suspend fun deleteProduct(productId: Long)

    //update product
    @Query("UPDATE product_entity SET name = :name, description=:desc, sale_price=:s_price, regular_price = :r_price, stores=:store, product_id=:productId WHERE name = :old_name")
    abstract suspend fun updateProduct(old_name:String,name:String,desc:String,s_price:String,r_price:String,store:String,productId:Long)
}