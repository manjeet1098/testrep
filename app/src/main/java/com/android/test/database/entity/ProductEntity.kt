package com.android.test.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "product_entity")
data class ProductEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    @ColumnInfo(name = "product_id")
    var productId: Long? = null,
    @ColumnInfo(name = "name")
    var name: String? = null,
    @ColumnInfo(name = "regular_price")
    var regularPrice: String? = null,
    @ColumnInfo(name = "sale_price")
    var salePrice: String? = null,
    @ColumnInfo(name = "description")
    var description: String? = null,
    @ColumnInfo(name = "product_photo")
    var productPhoto: String? = null,
    @ColumnInfo(name = "colors")
    var colors: String? = null,
    @ColumnInfo(name = "stores")
    var stores: String? = null
)