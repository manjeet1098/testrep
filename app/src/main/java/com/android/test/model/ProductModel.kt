package com.android.test.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProductModel(
    var productId: Long? = null,
    var name: String? = null,
    var regularPrice: String? = null,
    var salePrice: String? = null,
    var description: String? = null,
    var productPhoto: String? = null,
    var colors: String? = null,
    var stores: String? = null
) : Parcelable