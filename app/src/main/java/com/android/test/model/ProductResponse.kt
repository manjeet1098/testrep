package com.android.test.model


data class ProductResponse(
    val product: List<ProductModel>
)