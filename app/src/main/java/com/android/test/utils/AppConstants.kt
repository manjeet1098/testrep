package com.android.test.utils

/**
 *create constant file to define the constant in application
 */
class AppConstants {
    companion object {
        const val PRODUCT_DATA = "product_data"
        const val PRODUCT_ARRAY_KEY = "product"
        const val PRODUCT_ID_KEY = "productId"
        const val PRODUCT_NAME_KEY = "name"
        const val PRODUCT_DESCRIPTION_KEY = "description"
        const val PRODUCT_SALE_PRICE_KEY = "salePrice"
        const val PRODUCT_REGULAR_PRICE_KEY = "regularPrice"
        const val PRODUCT_STORE_KEY = "stores"
        const val PRODUCT_COLOR_KEY = "colors"
        const val PRODUCT_PHOTO_KEY = "productPhoto"


    }
}