package com.android.test.utils

import android.widget.Toast
import com.android.test.application.MyApplication


/**
 * Utility class
 */
class Utility {

    companion object {

        /**
         * Method to show the toast message
         */
        fun showToast(message: String) {
            Toast.makeText(MyApplication.instance, message, Toast.LENGTH_SHORT).show()
        }



    }
}
