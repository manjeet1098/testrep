package com.android.test.view.activity

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.android.test.BR
import com.android.test.R
import com.android.test.base.BaseActivityAVM
import com.android.test.databinding.ActivityCreateProductBinding
import com.android.test.utils.Utility
import com.android.test.viewmodel.CreateProductViewModel


/**
 * Used to create the products
 */
class CreateProductActivity :
    BaseActivityAVM<ActivityCreateProductBinding, CreateProductViewModel>() {

    private lateinit var mViewModel: CreateProductViewModel
    private lateinit var mViewBinding: ActivityCreateProductBinding


    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_create_product
    }

    override fun getViewModel(): CreateProductViewModel {
        val mCreateViewModel: CreateProductViewModel by viewModels()
        this.mViewModel = mCreateViewModel
        return this.mViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewBinding = getViewDataBinding()
        mViewBinding.viewModel = mViewModel
        setObserver()

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        finish()
        return true
    }

    /**
     * Observe the result
     */
    private fun setObserver() {
        mViewModel.displayProductButtonClicked.observe(this, Observer {
            goToProductListScreen()
        })
        mViewModel.createProductButtonClicked.observe(this, Observer {
            Utility.showToast(it)
        })

        mViewModel.onValidationFailed.observe(this, Observer {
            Utility.showToast(it)
        })

    }

    /**
     *  navigate to product list screen
     */
    private fun goToProductListScreen() {
        val intent = Intent(this, ProductListActivity::class.java)
        startActivity(intent)
    }


}
