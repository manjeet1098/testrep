package com.android.test.view.activity

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.android.test.BR
import com.android.test.R
import com.android.test.base.BaseActivityAVM
import com.android.test.databinding.ActivityDashboardBinding
import com.android.test.viewmodel.DashboardViewModel


/**
 * Used to set the payment details
 */
class DashboardActivity :
    BaseActivityAVM<ActivityDashboardBinding, DashboardViewModel>() {

    private lateinit var mViewModel: DashboardViewModel
    private lateinit var mViewBinding: ActivityDashboardBinding


    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_dashboard
    }

    override fun getViewModel(): DashboardViewModel {
        val mDashboardViewModel: DashboardViewModel by viewModels()
        this.mViewModel = mDashboardViewModel
        return this.mViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewBinding = getViewDataBinding()
        mViewBinding.viewModel = mViewModel
        setScreenTitle(
            getViewDataBinding().customToolbar.toolbar,
            getString(R.string.screen_title_dashboard)
        )
        setObserver()

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        finish()
        return true
    }

    /**
     * Observe the result
     */
    private fun setObserver() {
        mViewModel.displayProductButtonClicked.observe(this, Observer {
            goToProductListScreen()
        })
        mViewModel.createProductButtonClicked.observe(this, Observer {
            goToCreateProductScreen()
        })
    }

    /**
     *  navigate to create product
     */
    private fun goToCreateProductScreen() {
        val intent = Intent(this, CreateProductActivity::class.java)
        startActivity(intent)
    }

    /**
     *  navigate to product list
     */
    private fun goToProductListScreen() {
        val intent = Intent(this, ProductListActivity::class.java)
        startActivity(intent)
    }


}
