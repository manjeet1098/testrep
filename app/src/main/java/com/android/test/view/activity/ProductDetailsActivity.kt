package com.android.test.view.activity

import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.android.test.BR
import com.android.test.R
import com.android.test.base.BaseActivityAVM
import com.android.test.databinding.ActivityProductDetailBinding
import com.android.test.utils.AppConstants
import com.android.test.view.fragment.ZoomProductImageDialogFragment
import com.android.test.viewmodel.ProductDetailsViewModel


/**
 * Used to set products details
 */
class ProductDetailsActivity :
    BaseActivityAVM<ActivityProductDetailBinding, ProductDetailsViewModel>() {

    private lateinit var mViewModel: ProductDetailsViewModel
    private lateinit var mViewBinding: ActivityProductDetailBinding


    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_product_detail
    }

    override fun getViewModel(): ProductDetailsViewModel {
        val mJackpotTotalViewModel: ProductDetailsViewModel by viewModels()
        this.mViewModel = mJackpotTotalViewModel
        return this.mViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewBinding = getViewDataBinding()
        mViewBinding.viewModel = mViewModel
        mViewModel.setInitialData(intent.getParcelableExtra(AppConstants.PRODUCT_DATA)!!)

        setObservers()

    }
    /**
     * Observe the result
     */
    private fun setObservers() {
        mViewModel.onProductImageClicked.observe(this, Observer {
            ZoomProductImageDialogFragment(
                it
            ).show((this).supportFragmentManager, "add")

        })

    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        finish()
        return true
    }


}
