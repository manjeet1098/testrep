package com.android.test.view.activity

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.android.test.BR
import com.android.test.R
import com.android.test.base.BaseActivityAVM
import com.android.test.databinding.ActivityProductListBinding
import com.android.test.model.ProductModel
import com.android.test.utils.AppConstants.Companion.PRODUCT_DATA
import com.android.test.view.fragment.ZoomProductImageDialogFragment
import com.android.test.viewmodel.ProductListViewModel


/**
 * Used to set the payment list
 */
class ProductListActivity :
    BaseActivityAVM<ActivityProductListBinding, ProductListViewModel>() {

    private lateinit var mViewModel: ProductListViewModel
    private lateinit var mViewBinding: ActivityProductListBinding


    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_product_list
    }

    override fun getViewModel(): ProductListViewModel {
        val mProductListViewModel: ProductListViewModel by viewModels()
        this.mViewModel = mProductListViewModel
        return this.mViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewBinding = getViewDataBinding()
        mViewBinding.viewModel = mViewModel
        setObserver()

    }

    /**
     * Observe the result
     */
    private fun setObserver() {
        mViewModel.onUpdateProduct.observe(this, Observer {
            goToUpdateProductScreen(it)
        })
        mViewModel.onProductClicked.observe(this, Observer {
            goToProductDetailsScreen(it)
        })
        mViewModel.onCreateProductClicked.observe(this, Observer {
            goToCreateProductScreen()
        })
        mViewModel.onProductImageClicked.observe(this, Observer {
            ZoomProductImageDialogFragment(
                it
            ).show((this).supportFragmentManager, "add")

        })


    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        finish()
        return true
    }

    /**
     *  navigate to update product
     */
    private fun goToUpdateProductScreen(productModel: ProductModel) {
        val intent = Intent(this, UpdateProductActivity::class.java)
        intent.putExtra(PRODUCT_DATA, productModel)
        startActivity(intent)
        finish()
    }

    /**
     *  navigate to  product details
     */
    private fun goToProductDetailsScreen(productModel: ProductModel) {
        val intent = Intent(this, ProductDetailsActivity::class.java)
        intent.putExtra(PRODUCT_DATA, productModel)
        startActivity(intent)
        finish()
    }

    /**
     *  navigate to create product
     */
    private fun goToCreateProductScreen() {
        val intent = Intent(this, CreateProductActivity::class.java)
        startActivity(intent)
        finish()
    }


}
