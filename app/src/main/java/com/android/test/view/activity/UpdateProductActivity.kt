package com.android.test.view.activity

import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.android.test.BR
import com.android.test.R
import com.android.test.base.BaseActivityAVM
import com.android.test.databinding.ActivityUpdateProductBinding
import com.android.test.utils.AppConstants.Companion.PRODUCT_DATA
import com.android.test.utils.Utility
import com.android.test.viewmodel.UpdateProductViewModel


/**
 * Used to update product
 */
class UpdateProductActivity :
    BaseActivityAVM<ActivityUpdateProductBinding, UpdateProductViewModel>() {

    private lateinit var mViewModel: UpdateProductViewModel
    private lateinit var mViewBinding: ActivityUpdateProductBinding


    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_update_product
    }

    override fun getViewModel(): UpdateProductViewModel {
        val mJackpotTotalViewModel: UpdateProductViewModel by viewModels()
        this.mViewModel = mJackpotTotalViewModel
        return this.mViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewBinding = getViewDataBinding()
        mViewBinding.viewModel = mViewModel
        mViewModel.setInitialData(intent.getParcelableExtra(PRODUCT_DATA)!!)
        setObserver()

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        finish()
        return true
    }

    /**
     * Observe the result
     */
    private fun setObserver() {
        mViewModel.createProductButtonClicked.observe(this, Observer {
            Utility.showToast(it)
        })

        mViewModel.onValidationFailed.observe(this, Observer {
            Utility.showToast(it)
        })

    }


}
