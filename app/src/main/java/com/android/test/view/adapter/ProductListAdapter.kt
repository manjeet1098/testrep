package com.android.test.view.adapter


import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.test.base.BaseViewHolder
import com.android.test.database.AppDatabase
import com.android.test.databinding.ProductsListRowItemBinding
import com.android.test.model.ProductModel
import com.android.test.viewmodel.ProductListAdapterViewModel
import com.android.test.viewmodel.ProductListViewModel


/**
 * This adapter class is used to handle the product list data
 */
class ProductListAdapter(var productListViewModel: ProductListViewModel) : RecyclerView.Adapter<BaseViewHolder>() {
    var productList: ArrayList<ProductModel>? = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val mRowBinding = ProductsListRowItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        return ViewHolder(mRowBinding)
    }

    override fun getItemCount(): Int {
        return productList!!.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        return holder.onBind(position)
    }

    inner class ViewHolder(
        private val mRowItemBinding: ProductsListRowItemBinding
    ) : BaseViewHolder(itemView = mRowItemBinding.root) {
        private lateinit var mViewModel: ProductListAdapterViewModel

        override fun onBind(position: Int) {
            val product = productList?.get(position)
            mViewModel = ProductListAdapterViewModel(product,productListViewModel)
            mRowItemBinding.viewModel = mViewModel
            mViewModel.init()
            mRowItemBinding.executePendingBindings()
        }

    }

    /**
     * This will set the data in the list in adapter
     */
    fun setList(productList1: List<ProductModel>?) {
        productList!!.clear()
        productList1!!.forEach {
            productList!!.add(it)
        }
        notifyDataSetChanged()
    }
}