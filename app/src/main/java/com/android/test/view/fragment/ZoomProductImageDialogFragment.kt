package com.android.test.view.fragment


import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import com.android.test.R
import com.android.test.databinding.FragmentZoomProductImageBinding
import com.android.test.viewmodel.ZoomProductImageViewModel

/**
 * showing image as zoom
 */
class ZoomProductImageDialogFragment(
    private var image: Int

) : DialogFragment() {
    private lateinit var mViewModel: ZoomProductImageViewModel

    override
    fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val mBinding: FragmentZoomProductImageBinding = DataBindingUtil
            .inflate(
                LayoutInflater.from(context),
                R.layout.fragment_zoom_product_image,
                null,
                false
            )
        mViewModel = getViewModel()
        mBinding.viewModel = mViewModel
        mViewModel.imageUrl.value = image
        val dialog = Dialog(requireContext())
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(mBinding.root)
        if (dialog.window != null) {
            dialog.window!!.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }

    /**
     * Method to get view model
     */
    private fun getViewModel(): ZoomProductImageViewModel {
        val mPaymentConfirmationDialogViewModel: ZoomProductImageViewModel by viewModels()
        return mPaymentConfirmationDialogViewModel
    }




}