package com.android.test.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.android.test.R
import com.android.test.application.MyApplication
import com.android.test.base.BaseAndroidViewModel
import com.android.test.database.DatabaseRepository
import com.android.test.model.ProductModel
import com.android.test.utils.AppConstants
import com.google.gson.Gson
import org.json.JSONException
import org.json.JSONObject

/**
 * Used to handle create product
 */

class CreateProductViewModel(application: Application) : BaseAndroidViewModel(application) {
    var createProductButtonClicked: MutableLiveData<String> = MutableLiveData()
    var onValidationFailed: MutableLiveData<String> = MutableLiveData()
    var displayProductButtonClicked: MutableLiveData<String> = MutableLiveData()
    private val mDatabaseRepository = DatabaseRepository(mDB!!)

    // To store input data
    var name: MutableLiveData<String?> = MutableLiveData()
    var description: MutableLiveData<String?> = MutableLiveData()
    var salePrice: MutableLiveData<String> = MutableLiveData()
    var regularPrice: MutableLiveData<String?> = MutableLiveData()
    var stores: MutableLiveData<String?> = MutableLiveData()
    var productId: MutableLiveData<String> = MutableLiveData()


    /**
     * make the data in json object
     */

    private fun makeJsonObject(
        id: Long,
        name: String,
        description: String,
        salePrice: String,
        regularPrice: String,
        image: String,
        stores: String,
        colors: String
    ): JSONObject? {
        var obj: JSONObject? = null
        obj = JSONObject()
        try {

            obj.put(AppConstants.PRODUCT_ID_KEY, id)
            obj.put(AppConstants.PRODUCT_NAME_KEY, name)
            obj.put(AppConstants.PRODUCT_DESCRIPTION_KEY, description)
            obj.put(AppConstants.PRODUCT_SALE_PRICE_KEY, salePrice)
            obj.put(AppConstants.PRODUCT_REGULAR_PRICE_KEY, regularPrice)
            obj.put(AppConstants.PRODUCT_STORE_KEY, stores)
            obj.put(AppConstants.PRODUCT_COLOR_KEY, colors)
            obj.put(AppConstants.PRODUCT_PHOTO_KEY, image)


        } catch (e: JSONException) { // TODO Auto-generated catch block
            e.printStackTrace()
        }
        return obj
    }


    /**
     * @description Method is used to convert json data into respective POJO class.
     */
    private fun <T> getObject(response: String, instance: Class<T>): Any? {
        return Gson().fromJson(response, instance)
    }

    /**
     * method to handle submit click
     */
    fun onSubmitClicked() {
        when {
            productId.value.isNullOrEmpty() -> {
                onValidationFailed.value =
                    MyApplication.instance.getString(R.string.product_id_error)
            }
            name.value.isNullOrEmpty() -> {
                onValidationFailed.value =
                    MyApplication.instance.getString(R.string.name_error)
            }
            description.value.isNullOrEmpty() -> {
                onValidationFailed.value =
                    MyApplication.instance.getString(R.string.description_error)
            }

            salePrice.value.isNullOrEmpty() -> {


                onValidationFailed.value =
                    MyApplication.instance.getString(R.string.sales_price_error)
            }
            regularPrice.value.isNullOrEmpty() -> {
                onValidationFailed.value =
                    MyApplication.instance.getString(R.string.regular_price_error)
            }
            stores.value.isNullOrEmpty() -> {
                onValidationFailed.value =
                    MyApplication.instance.getString(R.string.stores_error)
            }
            else -> {
                val resultList = mDatabaseRepository.getProductList()
                val result = resultList!!.filter { s ->
                    s.name == name.value

                }
                if (result.isEmpty()) {
                    val result1 = getObject(
                        makeJsonObject(
                            productId.value!!.toLong(),
                            name.value!!,
                            description.value!!,
                            salePrice.value!!,
                            regularPrice.value!!,
                            "",
                            stores.value!!,
                            ""
                        ).toString(), ProductModel::class.java
                    )
                    if (result1 is ProductModel) {
                        mDatabaseRepository.insertProducts(result1)
                        createProductButtonClicked.value =
                            MyApplication.instance.getString(R.string.product_create_success)
                        setNullData()
                    }


                } else {
                    createProductButtonClicked.value =
                        MyApplication.instance.getString(R.string.product_already_exist)

                }

            }

        }

    }

    /**
     * set data to null
     */
    private fun setNullData() {
        productId.value = ""
        description.value = ""
        name.value = ""
        salePrice.value = ""
        regularPrice.value = ""
        stores.value = ""


    }

}