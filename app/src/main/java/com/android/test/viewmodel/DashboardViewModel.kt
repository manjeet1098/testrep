package com.android.test.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.android.test.base.BaseAndroidViewModel

/**
 * handle clicks on dashboard
 */

class DashboardViewModel(application: Application) : BaseAndroidViewModel(application) {
    var createProductButtonClicked: MutableLiveData<String> = MutableLiveData()
    var displayProductButtonClicked: MutableLiveData<String> = MutableLiveData()

    /**
     * handle clicks on create button
     */
    fun onCreateProductClicked() {
        createProductButtonClicked.value = ""
    }

    /**
     * handle clicks on display products
     */
    fun onDisplayProductClicked() {
        displayProductButtonClicked.value = ""
    }

}