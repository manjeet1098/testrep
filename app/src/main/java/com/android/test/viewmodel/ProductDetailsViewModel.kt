package com.android.test.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.android.test.R
import com.android.test.application.MyApplication
import com.android.test.base.BaseAndroidViewModel
import com.android.test.model.ProductModel

/**
 * This will handle products details data
 */

class ProductDetailsViewModel(application: Application) : BaseAndroidViewModel(application) {
    var name: MutableLiveData<String> = MutableLiveData()
    var salePrice: MutableLiveData<String> = MutableLiveData()
    var regularPrice: MutableLiveData<String> = MutableLiveData()
    var stores: MutableLiveData<String> = MutableLiveData()
    var description: MutableLiveData<String> = MutableLiveData()
    var onProductImageClicked: MutableLiveData<Int> = MutableLiveData()

    /**
     * This will set the data in product details
     */
    fun setInitialData(productModel: ProductModel) {
        salePrice.value =
            MyApplication.instance.getString(R.string.sale_price_label) + productModel.salePrice
        regularPrice.value =
            MyApplication.instance.getString(R.string.regular_price_label) + productModel.regularPrice
        stores.value =
            MyApplication.instance.getString(R.string.stores_label) + productModel.stores

        name.value = productModel.name
        description.value = productModel.description

    }

    /**
     * This will handle on click of create product image
     */
    fun onProductImageClicked() {
        onProductImageClicked.value = R.drawable.logo

    }


}