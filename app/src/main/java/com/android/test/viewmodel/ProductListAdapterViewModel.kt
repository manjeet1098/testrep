package com.android.test.viewmodel

import androidx.lifecycle.MutableLiveData
import com.android.test.R
import com.android.test.application.MyApplication
import com.android.test.model.ProductModel
import java.lang.Exception

/**
 * This adapter view model class is used to set data in product list
 */
class ProductListAdapterViewModel(
    private var mProduct: ProductModel?,
    var productListViewModel: ProductListViewModel
) {

    var name: MutableLiveData<String> = MutableLiveData()
    var productPhoto: MutableLiveData<String> = MutableLiveData()
    var salePrice: MutableLiveData<String> = MutableLiveData()
    var regularPrice: MutableLiveData<String> = MutableLiveData()


    fun init() {
        name.value = mProduct!!.name
        salePrice.value =
            MyApplication.instance.getString(R.string.sale_price_label) + mProduct!!.salePrice
        regularPrice.value =
            MyApplication.instance.getString(R.string.regular_price_label) + mProduct!!.regularPrice

        productPhoto.value = mProduct!!.productPhoto
    }

    /**
     * This will handle on click of update button
     */
    fun onUpdateClicked() {
        productListViewModel.onUpdateClicked(mProduct!!)
    }

    /**
     * This will handle on click of delete button
     */
    fun onDeleteClicked() {
        productListViewModel.onDeleteClicked(mProduct?.productId!!)

    }

    /**
     * This will handle on click of product
     */
    fun onProductClicked() {
        productListViewModel.onProductClicked(mProduct!!)

    }

    /**
     * This will handle on click of product image
     */
    fun onProductImageClicked() {
        try {
            productListViewModel.onProductImageClicked(R.drawable.logo)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


}