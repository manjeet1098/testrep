package com.android.test.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.android.test.base.BaseAndroidViewModel
import com.android.test.database.DatabaseRepository
import com.android.test.model.ProductModel
import com.android.test.view.adapter.ProductListAdapter

/**
 * This will set the adapter and handle product listing
 */

class ProductListViewModel(application: Application) : BaseAndroidViewModel(application) {
    var adapter: ProductListAdapter? = null
    private val mDatabaseRepository = DatabaseRepository(mDB!!)
    var onUpdateProduct: MutableLiveData<ProductModel> = MutableLiveData()
    var onProductClicked: MutableLiveData<ProductModel> = MutableLiveData()
    var isProductAvailable: MutableLiveData<Boolean> = MutableLiveData()
    var onCreateProductClicked: MutableLiveData<String> = MutableLiveData()
    var onProductImageClicked: MutableLiveData<Int> = MutableLiveData()

    init {
        adapter = ProductListAdapter(this)
        val list = mDatabaseRepository.getProductList()
        if (list.isNullOrEmpty()) {
            isProductAvailable.value = false
        } else {
            isProductAvailable.value = true
            adapter!!.setList(mDatabaseRepository.getProductList())
        }
    }

    /**
     * This will handle on click of delete button
     */
    fun onDeleteClicked(productId: Long) {
        mDatabaseRepository.deleteProducts(productId)
        adapter!!.setList(mDatabaseRepository.getProductList())
    }

    /**
     * This will handle on click of delete button
     */
    fun onUpdateClicked(productModel: ProductModel) {
        onUpdateProduct.value = productModel
    }

    /**
     * This will handle on click of product
     */
    fun onProductClicked(productModel: ProductModel) {
        onProductClicked.value = productModel
    }

    /**
     * This will handle on click of create product
     */
    fun createProductClicked() {
        onCreateProductClicked.value = ""
    }


    /**
     * This will handle on click of create product image
     */
    fun onProductImageClicked(productPhoto: Int?) {
        onProductImageClicked.value = productPhoto
    }

}