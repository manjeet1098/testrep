package com.android.test.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.android.test.R
import com.android.test.application.MyApplication
import com.android.test.base.BaseAndroidViewModel
import com.android.test.database.DatabaseRepository
import com.android.test.model.ProductModel
import com.google.gson.Gson
import org.json.JSONException
import org.json.JSONObject

/**
 * Used to handle update product
 */

class UpdateProductViewModel(application: Application) : BaseAndroidViewModel(application) {
    var createProductButtonClicked: MutableLiveData<String> = MutableLiveData()
    var onValidationFailed: MutableLiveData<String> = MutableLiveData()
    private val mDatabaseRepository = DatabaseRepository(mDB!!)

    // To store input data
    var name: MutableLiveData<String?> = MutableLiveData()
    var oldName: MutableLiveData<String?> = MutableLiveData()
    var description: MutableLiveData<String?> = MutableLiveData()
    var salePrice: MutableLiveData<String> = MutableLiveData()
    var regularPrice: MutableLiveData<String?> = MutableLiveData()
    var stores: MutableLiveData<String?> = MutableLiveData()
    var productId: MutableLiveData<String> = MutableLiveData()

    /**
     * This will set the data in update product fields
     */
    fun setInitialData(productModel: ProductModel) {
        oldName.value = productModel.name
        name.value = productModel.name
        description.value = productModel.description
        stores.value = productModel.stores
        salePrice.value = productModel.salePrice
        regularPrice.value = productModel.regularPrice
        productId.value = productModel.productId.toString()

    }

    /**
     * make the data in json object
     */

    private fun makJsonObject(
        id: Long,
        name: String,
        description: String,
        salePrice: String,
        regularPrice: String,
        image: String,
        stores: String,
        colors: String
    ): JSONObject? {
        var obj: JSONObject? = null
        obj = JSONObject()
        try {
            obj.put("productId", id)
            obj.put("name", name)
            obj.put("description", description)
            obj.put("salePrice", salePrice)
            obj.put("regularPrice", regularPrice)
            obj.put("productPhoto", image)
            obj.put("stores", stores)
            obj.put("colors", colors)

        } catch (e: JSONException) { // TODO Auto-generated catch block
            e.printStackTrace()
        }
        return obj
    }


    /**
     * @description Method is used to convert json data into respective POJO class.
     */
    private fun <T> getObject(response: String, instance: Class<T>): Any? {
        return Gson().fromJson(response, instance)
    }

    /**
     * method to handle submit click
     */
    fun onSubmitClicked() {
        when {
            productId.value.isNullOrEmpty() -> {
                onValidationFailed.value =
                    MyApplication.instance.getString(R.string.product_id_error)
            }
            name.value.isNullOrEmpty() -> {
                onValidationFailed.value =
                    MyApplication.instance.getString(R.string.name_error)
            }
            description.value.isNullOrEmpty() -> {
                onValidationFailed.value =
                    MyApplication.instance.getString(R.string.description_error)
            }

            salePrice.value.isNullOrEmpty() -> {
                onValidationFailed.value =
                    MyApplication.instance.getString(R.string.sales_price_error)
            }
            regularPrice.value.isNullOrEmpty() -> {
                onValidationFailed.value =
                    MyApplication.instance.getString(R.string.regular_price_error)
            }
            stores.value.isNullOrEmpty() -> {
                onValidationFailed.value =
                    MyApplication.instance.getString(R.string.stores_error)
            }
            else -> {
                val resultList = mDatabaseRepository.getProductList()
                val result = resultList!!.filter { s ->
                    s.name == name.value

                }
                if (result.isEmpty()) {
                    val result1 = getObject(
                        makJsonObject(
                            productId.value!!.toLong(),
                            name.value!!,
                            description.value!!,
                            salePrice.value!!,
                            regularPrice.value!!,
                            "",
                            stores.value!!,
                            ""
                        ).toString(), ProductModel::class.java
                    )
                    if (result1 is ProductModel) {
                        mDatabaseRepository.updateProduct(oldName.value!!, result1)
                        createProductButtonClicked.value =
                            MyApplication.instance.getString(R.string.product_update_success)
                        setNullData()
                    }


                } else {
                    createProductButtonClicked.value =
                        MyApplication.instance.getString(R.string.product_already_exist)

                }

            }

        }

    }

    /**
     * set data to null
     */
    private fun setNullData() {
        productId.value = ""
        description.value = ""
        name.value = ""
        salePrice.value = ""
        regularPrice.value = ""
        stores.value = ""


    }

}