package com.android.test.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.android.test.base.BaseAndroidViewModel

/**
 * This will handle zooming effect
 */

class ZoomProductImageViewModel(application: Application) : BaseAndroidViewModel(application) {
    var imageUrl: MutableLiveData<Int> = MutableLiveData()


}